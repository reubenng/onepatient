﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace OnePatient.Models
{
    public class Patient
    {
        public int ID { get; set; }
        public string NRIC { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Race { get; set; }
        public string Email { get; set; }
        public string Drug_Allergy { get; set; }
    }
}
