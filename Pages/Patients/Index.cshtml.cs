﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnePatient.Models;

namespace OnePatient.Pages.Patients
{
    public class IndexModel : PageModel
    {
        private readonly OnePatient.Models.OnePatientContext _context;

        public IndexModel(OnePatient.Models.OnePatientContext context)
        {
            _context = context;
        }

        public IList<Patient> Patient { get;set; }

        public async Task OnGetAsync()
        {
            Patient = await _context.Patient.ToListAsync();
        }
    }
}
