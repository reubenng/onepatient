﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnePatient.Models;

[assembly: HostingStartup(typeof(OnePatient.Areas.Identity.IdentityHostingStartup))]
namespace OnePatient.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<OnePatient>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("OnePatientConnection")));

                services.AddDefaultIdentity<IdentityUser>()
                    .AddEntityFrameworkStores<OnePatient>();
            });
        }
    }
}